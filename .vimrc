set guifont=Fira\ Code
set guioptions=agit

set t_Co=256

set nocompatible
syntax enable
filetype plugin indent on
set autoindent
set expandtab
set hidden
set encoding=utf-8
set hlsearch
set history=1000
set number
set relativenumber
set scrolloff=7
set wildmenu
set foldcolumn=1

set undofile
set undodir=$HOME/vimundo/
set undolevels=1000

map <f2> :w<cr>
imap <f2> <esc>:w<cr>l
vmap <f2> <esc>:w<cr>

let mapleader=","

map <leader>s :setlocal spell!<cr>
map <leader>y :%y+<cr>:%y<cr>
map <leader>q :q<cr>


call plug#begin('~/.vim/plugged')
Plug 'scrooloose/nerdtree'
Plug 'vim-airline/vim-airline'
Plug 'joshdick/onedark.vim'
call plug#end()


"Use 24-bit (true-color) mode in Vim/Neovim when outside tmux.
"If you're using tmux version 2.2 or later, you can remove the outermost
"$TMUX check and use tmux's 24-bit color support
"(see < http://sunaku.github.io/tmux-24bit-color.html#usage > for more
"information.)
if (empty($TMUX))
  if (has("nvim"))
    "For Neovim 0.1.3 and 0.1.4 <https://github.com/neovim/neovim/pull/2198 >
    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  endif
  "For Neovim > 0.1.5 and Vim > patch 7.4.1799 <https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162>
  "Based on Vim patch 7.4.1770 (`guicolors` option) <https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd>
  "<https://github.com/neovim/neovim/wiki/Following-HEAD#20160511>
  if (has("termguicolors"))
    set termguicolors
  endif
endif

let g:airline_theme='onedark'
let g:onedark_terminal_italics=1
set background=dark
colorscheme onedark
